﻿using Prism.Events;
using Prism.Modularity;
using Prism.Regions;
using PW.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PW.SystemHeader
{
    /// <summary>
    /// HeaderView.xaml 的交互逻辑
    /// </summary>
    [Export(typeof(HeaderView))]
    public partial class HeaderView : UserControl, INavigationAware
    {
        [Import]
        public IRegionManager regionManager;
        [Import]
        public IModuleManager moduleManager;

        string initCheckModule = "";

        [ImportingConstructor]
        public HeaderView(IRegionManager regionManager, IEventAggregator eventAggregator, IModuleManager moduleManager)
        {
            HNavigateToScreenEvent ntsEvent = GlobalData.EventAggregator.GetEvent<HNavigateToScreenEvent>();
            ntsEvent.Subscribe(OnLinkageNavigateEvent);
        }
        public void OnLinkageNavigateEvent(CommandRegionEventArgs e)
        {
            initCheckModule = e.Module;
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            //throw new System.NotImplementedException();
        }

        public void OnNavigatedTo(NavigationContext navigationContext)
        {

            InitializeComponent();
            if (navigationContext.Parameters["Module"] != null)
            {
                initCheckModule = navigationContext.Parameters["Module"].ToString();
            }
            List<NavModuleInfo> list = GlobalData.NavModules;
            btnStackPanel.Children.Clear();
            if (list != null)
            {
                for(int i=0;i<list.Count;i++){
                    btnStackPanel.Children.Add(createBtn(list[i], i));
                }
                //initBtn(navBtn1, navIco1, navTxt1, 0, list);
                //initBtn(navBtn2, navIco2, navTxt2, 1, list);
                //initBtn(navBtn3, navIco3, navTxt3, 2, list);
                //initBtn(navBtn4, navIco4, navTxt4, 3, list);
                //initBtn(navBtn5, navIco5, navTxt5, 4, list);
                //initBtn(navBtn6, navIco6, navTxt6, 5, list);
                //initBtn(navBtn7, navIco7, navTxt7, 6, list);
            }
            btnStackPanel.Children.Add(createEndBtn());
        }

        private void initBtn(ToggleButton navBtn, TextBlock navIco, TextBlock navTxt, int index, List<NavModuleInfo> list)
        {
            if (list.Count > index)
            {
                navBtn.Tag = list[index];
                navIco.Text = list[index].icon;
                navTxt.Text = list[index].title;
                if (initCheckModule == list[index].module)
                {
                    navBtn.IsChecked = true;
                }
            }
            else
            {
                navBtn.Visibility = Visibility.Collapsed;
            }
        }
        ToggleButton createBtn(NavModuleInfo nmi, int i) {
            //< ToggleButton Tag = "MapModule" x: Name = "navBtn1"  Width = "134" Height = "37"    Click = "SystemButton_Click"  Style = "{StaticResource ToggleButtonFirstStyle}" >
            //                    < Grid >
            //                        < Grid.ColumnDefinitions >
            //                            < ColumnDefinition ></ ColumnDefinition >
            //                            < ColumnDefinition ></ ColumnDefinition >
            //                        </ Grid.ColumnDefinitions >
            //                        < TextBlock x: Name = "navIco1" Style = "{DynamicResource FIcon}" ></ TextBlock >
            //                            < !--< Path x: Name = "navIco1" HorizontalAlignment = "center" Margin = "4,0,4,0" VerticalAlignment = "Center" Width = "22.3752" Height = "22.3726" Stretch = "Fill" Fill = "#FFC9DEFF" Data = "F1 M 910.682,39.628C 912.146,41.5193 913.017,43.8923 913.017,46.4688L 903.549,46.4688L 910.682,39.628 Z M 901.494,46.2746L 901.561,46.3445L 901.596,57.6541C 895.525,57.53 890.642,52.5698 890.642,46.469C 890.642,40.2903 895.651,35.2815 901.829,35.2815C 904.869,35.2815 907.625,36.4937 909.642,38.4611L 901.494,46.2746 Z M 912.909,48.0313C 912.209,53.0374 908.197,56.9819 903.158,57.5783L 903.129,48.0313L 912.909,48.0313 Z " UseLayoutRounding = "False" /> -->
            //                                                 < TextBlock x: Name = "navTxt1" Text = "xxx"  HorizontalAlignment = "Center" Grid.Column = "1" FontFamily = "微软雅黑" FontSize = "15"  VerticalAlignment = "Center" ></ TextBlock >
            //                                                           </ Grid >
            //                                                       </ ToggleButton >
            Grid content = new Grid();
            content.ColumnDefinitions.Add(new ColumnDefinition());
            content.ColumnDefinitions.Add(new ColumnDefinition());
            TextBlock icon = new TextBlock();
            icon.Text = nmi.icon;
            icon.SetResourceReference(StyleProperty, "FIcon");
            content.Children.Add(icon);
            TextBlock text = new TextBlock();
            text.Text = nmi.title;
            text.HorizontalAlignment = HorizontalAlignment.Center;
            text.VerticalAlignment = VerticalAlignment.Center;
            FontFamily font = new FontFamily("微软雅黑");
            text.FontFamily = font;
            text.FontSize = 15;
            text.SetValue(Grid.ColumnProperty, 1);
            content.Children.Add(text);
            ToggleButton btn = new ToggleButton();
            btn.Click += SystemButton_Click;
            btn.Tag = nmi;
            btn.Width = 134;
            btn.Height = 37;
            if (i == 0)
            {
                btn.Style = (Style)this.FindResource("ToggleButtonFirstStyle");
            }
            else {
                btn.Style = (Style)this.FindResource("ToggleButtonCenterStyle");
            }
            btn.Content = content;
            btn.IsChecked = false;
            if (initCheckModule == nmi.module)
            {
                btn.IsChecked = true;
            }

            return btn;
        }

        ToggleButton createEndBtn()
        {
            ToggleButton btn = new ToggleButton();
            btn.Style = (Style)this.FindResource("ToggleButtonLastStyle");
            return btn;
        }
        private void SystemButton_Click(object sender, RoutedEventArgs e)
        {
            UncheckedButtons();
            (sender as ToggleButton).IsChecked = true;
            CommandRegionEventArgs cms = new CommandRegionEventArgs();
            cms.Region = RegionNames.Main;
            cms.Module = ((sender as ToggleButton).Tag as NavModuleInfo).module;
            cms.menuVm = ((sender as ToggleButton).Tag as NavModuleInfo).menuVm;
            GlobalData.EventAggregator.GetEvent<HNavigateToScreenEvent>().Publish(cms);
        }
        private void UncheckedButtons()
        {
            foreach (var v in btnStackPanel.Children)
            {
                if (v is ToggleButton)
                {
                    ToggleButton tb = v as ToggleButton;
                    tb.IsChecked = false;
                }
            }
        }

        private void headerBtn_Click(object sender, RoutedEventArgs e)
        {
            if ((sender as ToggleButton).IsChecked.Value)
            {
                Storyboard sbHide = this.Resources["sb_HideRightPart"] as Storyboard;
                if (sbHide != null)
                    sbHide.Begin();
            }
            else
            {
                Storyboard sbHide = this.Resources["sb_ShowRightPart"] as Storyboard;
                if (sbHide != null)
                    sbHide.Begin();
            }
        }
    }
}
