
using PW.Common;
using PW.ServiceCenter;
using PW.ServiceCenter.ServiceSysMenu;
using System.ComponentModel;
using System.Windows;

namespace PW.SystemSet.ViewModel
{

    public class SysMenuViewModel : INotifyPropertyChanged
    {
        //构造函数
        public SysMenuViewModel(sys_menu record, bool disabled)
        {
            this._disabled = disabled;
            SubmitCommand = new RelayCommand(SubmitCommandFunc);
            CancelCommand = new RelayObjCommand(CancelCommandFunc);
            this._formObj = record;
						  			  if (record != null && record.menu_id != null) {
			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  							  			  GetInfo(record.menu_id);
			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			  			              }
        }

        #region 查询相关属性
        private sys_menu _formObj = new sys_menu();
        /// <summary>
        /// Username
        ///</summary>
        public sys_menu FormObj
        {
            get { return _formObj; }
            set
            {
                _formObj = value;
                RaisePropertyChanged("FormObj");
            }
        }
				        private void GetInfo(long id)
																																																																								        {
            CServiceSysMenu client = new CServiceSysMenu();
            client.getByIdCompleted += (serice, eve) =>
            {
                if (eve.Succesed)
                {
                    this._formObj = eve.Result;
                }
                else
                {
                    this._formObj = null;
                }
            };
          
            client.getById(id);
        }
        #endregion

        #region 分页相关属性
        private string _title = "编辑";
        /// <summary>
        /// 标题
        ///</summary>
        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                RaisePropertyChanged("Title");
            }
        }

        private bool _disabled = false;
        /// <summary>
        /// 是否禁用
        ///</summary>
        public bool Disabled
        {
            get { return _disabled; }
            set
            {
                _disabled = value;
                RaisePropertyChanged("Disabled");
            }
        }
        public Visibility Visible
        {
            get { return _disabled ? Visibility.Hidden : Visibility.Visible; }
        }

        public bool Enabled
        {
            get { return !_disabled; }
        }
        #endregion

        #region 按钮命令
        public RelayCommand SubmitCommand { get; set; }
        private void SubmitCommandFunc()
        {
									 if (_formObj.menu_id != null)
																																																																																																												           
            {
                CServiceSysMenu client = new CServiceSysMenu();
                client.updateByIdCompleted += (serice, eve) =>
                {
                    if (eve.Succesed)
                    {
                        MessageBoxX.Success("保存成功！");
                    }
                    else
                    {
                        MessageBoxX.Error("保存失败！");
                    }
                };
                client.updateById(_formObj);
            }
            else {
                CServiceSysMenu client = new CServiceSysMenu();
                client.addCompleted += (serice, eve) =>
                {
                    if (eve.Succesed)
                    {
                        MessageBoxX.Success("保存成功！");
                    }
                    else
                    {
                        MessageBoxX.Error("保存失败！");
                    }
                };
                client.add(_formObj);
            }
        }
        public RelayObjCommand CancelCommand { get; set; }
        private void CancelCommandFunc(object o)
        {
            (o as Window).Close();
        }
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
