﻿
using PW.Common;
using PW.ServiceCenter;
using PW.ServiceCenter.ServiceSysUser;
using PW.SystemSet.Views;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Linq;
using System.Collections.Generic;

namespace PW.SystemSet.ViewModel
{

    public class SysUserListViewModel : INotifyPropertyChanged
    {
        //构造函数
        public SysUserListViewModel()
        {
            QueryCommand = new RelayCommand(QueryCommandFunc);
            NextPageSearchCommand = new RelayCommand(NextPageSearchCommandFunc);
            SelectAllCommand = new RelayCommand(SelectAllCommandFunc);
            UnSelectAllCommand = new RelayCommand(UnSelectAllCommandFunc);
            AddCommand = new RelayCommand(AddCommandFunc);
            DeleteCommand = new RelayCommand(DeleteCommandFunc);
            ModifyCommand = new RelayCommand(ModifyCommandFunc);
            InfoCommand = new RelayCommand(InfoCommandFunc);
            GetData();
        }

        #region 查询相关属性
        private sys_user _queryObj = new sys_user();
        /// <summary>
        /// Username
        ///</summary>
        public sys_user QueryObj
        {
            get { return _queryObj; }
            set
            {
                _queryObj = value;
                RaisePropertyChanged("QueryObj");
            }
        }

        public RelayCommand QueryCommand { get; set; }

        private void QueryCommandFunc()
        {
            CurrentPage = "1";
            GetData();
        }

        //数据源
        ObservableCollection<DataGridModel<sys_user>> _list = new ObservableCollection<DataGridModel<sys_user>>();
        public ObservableCollection<DataGridModel<sys_user>> list
        {

            get { return _list; }
            set
            {
                _list = value;
                RaisePropertyChanged("list");
            }
        }
        private void GetData()
        {
            var pageIndex = Convert.ToInt32(CurrentPage);
            CServiceSysUser client = new CServiceSysUser();
            client.queryPageCompleted += (serice, eve) =>
            {
                if (eve.Succesed)
                {
                    list.Clear();
                    PageInfoOfsys_userCLUigIiY result = eve.Result;
                    this.TotalPage = result.totalPage.Value;
                    this.TotalCount = result.totalCount.Value;
                    foreach (sys_user item in result.list)
                    {
                        list.Add(new DataGridModel<sys_user>() { IsChecked = false, ObjData = item });
                    }
                }
                else
                {
                }
            };
            PageInfoOfsys_userCLUigIiY page = new PageInfoOfsys_userCLUigIiY()
            {
                pageIndex = pageIndex,
                pageSize = PageSize,
                orderName = "user_id ASC",
                queryParams = _queryObj
            };
            client.queryPage(JSONCom.ConvertObject<PageInfoOfsys_userCLUigIiY>(page));
        }
        #endregion

        #region 分页相关属性
        /// <summary>
        /// 分页管理
        ///</summary>
        public RelayCommand NextPageSearchCommand { get; set; }
        /// <summary>
        /// 分页查询命令
        ///</summary>
        private void NextPageSearchCommandFunc()
        {
            GetData();
        }
        private int _totalPage = 0;
        /// <summary>
        /// 总页数
        ///</summary>
        public int TotalPage
        {
            get { return _totalPage; }
            set
            {
                _totalPage = value;
                RaisePropertyChanged("TotalPage");
            }
        }

        private string _currentPage = "1";
        /// <summary>
        /// 当前页
        ///</summary>
        public string CurrentPage
        {
            get { return _currentPage; }
            set
            {
                _currentPage = value;
                RaisePropertyChanged("CurrentPage");
            }
        }

        private int _pageSize = 10;
        /// <summary>
        /// 每页显示的记录数
        ///</summary>
        public int PageSize
        {
            get { return _pageSize; }
            set
            {
                _pageSize = value;
                RaisePropertyChanged("PageSize");
            }
        }
        //private int _pageIndex = 1;
        //public int PageIndex
        //{
        //    get { return _pageIndex; }
        //    set
        //    {
        //        _pageIndex = value;

        //        RaisePropertyChanged("PageIndex");
        //    }
        //}
        private int _totalCount;
        public int TotalCount
        {
            get { return _totalCount; }
            set
            {
                _totalCount = value;
                RaisePropertyChanged("TotalCount");
            }
        }
        #endregion

        #region datagrid全选
        private bool? _IsCheckedAll = false;
        /// <summary>
        ///
        ///</summary>
        public bool? IsCheckedAll
        {
            get { return _IsCheckedAll; }
            set
            {
                _IsCheckedAll = value;
                RaisePropertyChanged("IsCheckedAll");
            }
        }

        public RelayCommand SelectAllCommand { get; set; }
        private void SelectAllCommandFunc()
        {
            foreach (DataGridModel<sys_user>
              item in list)
            {
                item.IsChecked = true;
            }
        }
        public RelayCommand UnSelectAllCommand { get; set; }
        private void UnSelectAllCommandFunc()
        {
            foreach (DataGridModel<sys_user> item in list)
            {
                item.IsChecked = false;
            }
        }
        #endregion

        #region 增删查改命令
        public RelayCommand AddCommand { get; set; }
        private void AddCommandFunc()
        {
            UserEdit edit = new UserEdit(new sys_user(), false);
            edit.ShowDialog();
        }
        public RelayCommand DeleteCommand { get; set; }
        private void DeleteCommandFunc()
        {
            sys_user record = getSelectRecord();
            if (record == null)
            {
                MessageBoxX.Warning("请选择一条数据");
                return;
            }
            // 支持多选
            bool res = MessageBoxX.Question("确认删除所选数据？");
            if (res) {
                CServiceSysUser client = new CServiceSysUser();
                client.deleteByIdCompleted += (serice, eve) =>
                {
                    if (eve.Succesed)
                    {
                        MessageBoxX.Success("删除成功！");
                    }
                    else
                    {
                        MessageBoxX.Error("删除失败！");
                    }
                };
                client.deleteById((int)record.user_id.Value);
            }
        }
        public RelayCommand ModifyCommand { get; set; }
        private void ModifyCommandFunc()
        {
            sys_user record = getSelectRecord();
            if (record == null)
            {
                MessageBoxX.Warning("请选择一条数据");
                return;
            }
            UserEdit edit = new UserEdit(record, false);
            edit.ShowDialog();
        }
        private sys_user getSelectRecord() {
            List<DataGridModel<sys_user>> records = _list.Where(item => item.IsChecked).ToList();
            if (records != null && records.Count > 0)
            {
                return records[0].ObjData;
            }
            return null;
        }
        private List<DataGridModel<sys_user>> getSelectRecords()
        {
            List<DataGridModel<sys_user>> records = _list.Where(item => item.IsChecked).ToList();
            return records;
        }
        public RelayCommand InfoCommand { get; set; }
        private void InfoCommandFunc()
        {
            sys_user record = getSelectRecord();
            if (record == null)
            {
                MessageBoxX.Warning("请选择一条数据");
                return;
            }
            UserEdit edit = new UserEdit(record, true);
            edit.ShowDialog();
        }
        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
