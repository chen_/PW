using PW.Controls.Controls;
using PW.ServiceCenter.ServiceSysMenu;
using PW.SystemSet.ViewModel;
using System.Windows;

namespace PW.SystemSet.Views
{
    /// <summary>
    /// SysMenuEdit.xaml 的交互逻辑
    /// </summary>
    public partial class SysMenuEdit : WindowBase
    {
        sys_menu record = null;
        bool disabled = false;
        public SysMenuEdit(sys_menu record, bool disabled)
        {
            this.record = record;
            InitializeComponent();
            this.DataContext = new SysMenuViewModel(record, disabled);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
        }
    }
}
