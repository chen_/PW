using PW.DBCommon.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace PW.DBCommon.Dao
{

public class SysConfigDao : BaseDao
{
  /// <summary>
  /// 查询
  /// </summary>
  public List<sys_config> query(sys_config record)
  {
    using (pwEntities myDb = new pwEntities())
    {
      IQueryable<sys_config>
      db = myDb.sys_config;
      if (record != null)
      {
                    if (record.config_id != null)
              {
        db = db.Where<sys_config>(p => p.config_id
		.Equals(record.config_id));
        }
                    if (!String.IsNullOrEmpty(record.config_name))
              {
        db = db.Where<sys_config>(p => p.config_name
		.Equals(record.config_name));
        }
                    if (!String.IsNullOrEmpty(record.config_key))
              {
        db = db.Where<sys_config>(p => p.config_key
		.Equals(record.config_key));
        }
                    if (!String.IsNullOrEmpty(record.config_value))
              {
        db = db.Where<sys_config>(p => p.config_value
		.Equals(record.config_value));
        }
                    if (!String.IsNullOrEmpty(record.config_type))
              {
        db = db.Where<sys_config>(p => p.config_type
		.Equals(record.config_type));
        }
                    if (!String.IsNullOrEmpty(record.create_by))
              {
        db = db.Where<sys_config>(p => p.create_by
		.Equals(record.create_by));
        }
                    if (record.create_time != null)
              {
        db = db.Where<sys_config>(p => p.create_time
		.Equals(record.create_time));
        }
                    if (!String.IsNullOrEmpty(record.update_by))
              {
        db = db.Where<sys_config>(p => p.update_by
		.Equals(record.update_by));
        }
                    if (record.update_time != null)
              {
        db = db.Where<sys_config>(p => p.update_time
		.Equals(record.update_time));
        }
                    if (!String.IsNullOrEmpty(record.remark))
              {
        db = db.Where<sys_config>(p => p.remark
		.Equals(record.remark));
        }
            }
      return db.ToList();
    }
  }

    /// <summary>
    /// 分页查询
    /// </summary>
    public PageInfo<sys_config> queryPage(PageInfo<sys_config> page)
    {
      Expression<Func<sys_config, bool>> whereLambda = PredicateExtensions.True<sys_config>();
      sys_config record = page.queryParams;
      if (record != null)
      {
                    if (record.config_id != null)
              {
          whereLambda.And(p => p.config_id
		  .Equals(record.config_id));
        }
                    if (!String.IsNullOrEmpty(record.config_name))
              {
          whereLambda.And(p => p.config_name
		  .Equals(record.config_name));
        }
                    if (!String.IsNullOrEmpty(record.config_key))
              {
          whereLambda.And(p => p.config_key
		  .Equals(record.config_key));
        }
                    if (!String.IsNullOrEmpty(record.config_value))
              {
          whereLambda.And(p => p.config_value
		  .Equals(record.config_value));
        }
                    if (!String.IsNullOrEmpty(record.config_type))
              {
          whereLambda.And(p => p.config_type
		  .Equals(record.config_type));
        }
                    if (!String.IsNullOrEmpty(record.create_by))
              {
          whereLambda.And(p => p.create_by
		  .Equals(record.create_by));
        }
                    if (record.create_time != null)
              {
          whereLambda.And(p => p.create_time
		  .Equals(record.create_time));
        }
                    if (!String.IsNullOrEmpty(record.update_by))
              {
          whereLambda.And(p => p.update_by
		  .Equals(record.update_by));
        }
                    if (record.update_time != null)
              {
          whereLambda.And(p => p.update_time
		  .Equals(record.update_time));
        }
                    if (!String.IsNullOrEmpty(record.remark))
              {
          whereLambda.And(p => p.remark
		  .Equals(record.remark));
        }
            }

      int pageSize = 10;
      if (page.pageSize != null)
      {
        pageSize = page.pageSize.Value;
       }
      int pageIndex = 1;
      if (page.pageIndex != null)
      {
        pageIndex = page.pageIndex.Value;
      }
            	  LoadPageItems(page, whereLambda, p => p.config_id, true);
                                                                                                                              return page;
      // return LoadPageItems(5, 2, out _total, whereLambda, p => p.id, true);
    }

    /// <summary>
    /// 删除
    /// </summary>
    public int deleteById(int id)
    {
      using (pwEntities myDb = new pwEntities())
      {
        // TODO 生成代码后需要检查一下是否找到正确的主键，这里做一个错误代码，避免直接使用
                    sys_config record = new sys_config() { config_id = id };
                                                                                                                              myDb.sys_config.Attach(record);
      myDb.Entry(record).State = EntityState.Deleted;
      return myDb.SaveChanges();
      }
    }

    /// <summary>
    /// 添加
    /// </summary>
    public int add(sys_config record)
    {
      using (pwEntities myDb = new pwEntities())
      {
        myDb.sys_config.Add(record);
        return myDb.SaveChanges();
      }
    }

    /// <summary>
    /// 更新
    /// </summary>
    public int updateById(sys_config record)
    {
      using (pwEntities myDb = new pwEntities())
      {
        myDb.sys_config.Attach(record);
        myDb.Entry(record).State = EntityState.Modified;
        return myDb.SaveChanges();
      }
    }

    /// <summary>
    /// getById
    /// </summary>
     public sys_config getById(int id)
     {
       using (pwEntities myDb = new pwEntities())
       {
     // TODO 生成代码后需要检查一下是否找到正确的主键，这里做一个错误代码，避免直接使用
                  return myDb.Set<sys_config>().Where<sys_config>(p => p.config_id == id).FirstOrDefault<sys_config>();
                                                                                                                                       

         }
      }
    }
}
