using PW.DBCommon.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace PW.DBCommon.Dao
{

public class SysMenuDao : BaseDao
{
  /// <summary>
  /// 查询
  /// </summary>
  public List<sys_menu> query(sys_menu record)
  {
    using (pwEntities myDb = new pwEntities())
    {
      IQueryable<sys_menu>
      db = myDb.sys_menu;
      if (record != null)
      {
                    if (record.menu_id != null)
              {
        db = db.Where<sys_menu>(p => p.menu_id
		.Equals(record.menu_id));
        }
                    if (!String.IsNullOrEmpty(record.menu_name))
              {
        db = db.Where<sys_menu>(p => p.menu_name
		.Equals(record.menu_name));
        }
                    if (record.parent_id != null)
              {
        db = db.Where<sys_menu>(p => p.parent_id
		.Equals(record.parent_id));
        }
                    if (record.order_num != null)
              {
        db = db.Where<sys_menu>(p => p.order_num
		.Equals(record.order_num));
        }
                    if (!String.IsNullOrEmpty(record.path))
              {
        db = db.Where<sys_menu>(p => p.path
		.Equals(record.path));
        }
                    if (!String.IsNullOrEmpty(record.component))
              {
        db = db.Where<sys_menu>(p => p.component
		.Equals(record.component));
        }
                    if (record.is_frame != null)
              {
        db = db.Where<sys_menu>(p => p.is_frame
		.Equals(record.is_frame));
        }
                    if (record.is_cache != null)
              {
        db = db.Where<sys_menu>(p => p.is_cache
		.Equals(record.is_cache));
        }
                    if (!String.IsNullOrEmpty(record.menu_type))
              {
        db = db.Where<sys_menu>(p => p.menu_type
		.Equals(record.menu_type));
        }
                    if (!String.IsNullOrEmpty(record.visible))
              {
        db = db.Where<sys_menu>(p => p.visible
		.Equals(record.visible));
        }
                    if (!String.IsNullOrEmpty(record.status))
              {
        db = db.Where<sys_menu>(p => p.status
		.Equals(record.status));
        }
                    if (!String.IsNullOrEmpty(record.perms))
              {
        db = db.Where<sys_menu>(p => p.perms
		.Equals(record.perms));
        }
                    if (!String.IsNullOrEmpty(record.icon))
              {
        db = db.Where<sys_menu>(p => p.icon
		.Equals(record.icon));
        }
                    if (!String.IsNullOrEmpty(record.create_by))
              {
        db = db.Where<sys_menu>(p => p.create_by
		.Equals(record.create_by));
        }
                    if (record.create_time != null)
              {
        db = db.Where<sys_menu>(p => p.create_time
		.Equals(record.create_time));
        }
                    if (!String.IsNullOrEmpty(record.update_by))
              {
        db = db.Where<sys_menu>(p => p.update_by
		.Equals(record.update_by));
        }
                    if (record.update_time != null)
              {
        db = db.Where<sys_menu>(p => p.update_time
		.Equals(record.update_time));
        }
                    if (!String.IsNullOrEmpty(record.remark))
              {
        db = db.Where<sys_menu>(p => p.remark
		.Equals(record.remark));
        }
            }
      return db.ToList();
    }
  }

    /// <summary>
    /// 分页查询
    /// </summary>
    public PageInfo<sys_menu> queryPage(PageInfo<sys_menu> page)
    {
      Expression<Func<sys_menu, bool>> whereLambda = PredicateExtensions.True<sys_menu>();
      sys_menu record = page.queryParams;
      if (record != null)
      {
                    if (record.menu_id != null)
              {
          whereLambda.And(p => p.menu_id
		  .Equals(record.menu_id));
        }
                    if (!String.IsNullOrEmpty(record.menu_name))
              {
          whereLambda.And(p => p.menu_name
		  .Equals(record.menu_name));
        }
                    if (record.parent_id != null)
              {
          whereLambda.And(p => p.parent_id
		  .Equals(record.parent_id));
        }
                    if (record.order_num != null)
              {
          whereLambda.And(p => p.order_num
		  .Equals(record.order_num));
        }
                    if (!String.IsNullOrEmpty(record.path))
              {
          whereLambda.And(p => p.path
		  .Equals(record.path));
        }
                    if (!String.IsNullOrEmpty(record.component))
              {
          whereLambda.And(p => p.component
		  .Equals(record.component));
        }
                    if (record.is_frame != null)
              {
          whereLambda.And(p => p.is_frame
		  .Equals(record.is_frame));
        }
                    if (record.is_cache != null)
              {
          whereLambda.And(p => p.is_cache
		  .Equals(record.is_cache));
        }
                    if (!String.IsNullOrEmpty(record.menu_type))
              {
          whereLambda.And(p => p.menu_type
		  .Equals(record.menu_type));
        }
                    if (!String.IsNullOrEmpty(record.visible))
              {
          whereLambda.And(p => p.visible
		  .Equals(record.visible));
        }
                    if (!String.IsNullOrEmpty(record.status))
              {
          whereLambda.And(p => p.status
		  .Equals(record.status));
        }
                    if (!String.IsNullOrEmpty(record.perms))
              {
          whereLambda.And(p => p.perms
		  .Equals(record.perms));
        }
                    if (!String.IsNullOrEmpty(record.icon))
              {
          whereLambda.And(p => p.icon
		  .Equals(record.icon));
        }
                    if (!String.IsNullOrEmpty(record.create_by))
              {
          whereLambda.And(p => p.create_by
		  .Equals(record.create_by));
        }
                    if (record.create_time != null)
              {
          whereLambda.And(p => p.create_time
		  .Equals(record.create_time));
        }
                    if (!String.IsNullOrEmpty(record.update_by))
              {
          whereLambda.And(p => p.update_by
		  .Equals(record.update_by));
        }
                    if (record.update_time != null)
              {
          whereLambda.And(p => p.update_time
		  .Equals(record.update_time));
        }
                    if (!String.IsNullOrEmpty(record.remark))
              {
          whereLambda.And(p => p.remark
		  .Equals(record.remark));
        }
            }

      int pageSize = 10;
      if (page.pageSize != null)
      {
        pageSize = page.pageSize.Value;
       }
      int pageIndex = 1;
      if (page.pageIndex != null)
      {
        pageIndex = page.pageIndex.Value;
      }
            	  LoadPageItems(page, whereLambda, p => p.menu_id, true);
                                                                                                                                                                                                                              return page;
      // return LoadPageItems(5, 2, out _total, whereLambda, p => p.id, true);
    }

    /// <summary>
    /// 删除
    /// </summary>
    public int deleteById(long id)
    {
      using (pwEntities myDb = new pwEntities())
      {
        // TODO 生成代码后需要检查一下是否找到正确的主键，这里做一个错误代码，避免直接使用
                    sys_menu record = new sys_menu() { menu_id = id };
                                                                                                                                                                                                                              myDb.sys_menu.Attach(record);
      myDb.Entry(record).State = EntityState.Deleted;
      return myDb.SaveChanges();
      }
    }

    /// <summary>
    /// 添加
    /// </summary>
    public int add(sys_menu record)
    {
      using (pwEntities myDb = new pwEntities())
      {
        myDb.sys_menu.Add(record);
        return myDb.SaveChanges();
      }
    }

    /// <summary>
    /// 更新
    /// </summary>
    public int updateById(sys_menu record)
    {
      using (pwEntities myDb = new pwEntities())
      {
        myDb.sys_menu.Attach(record);
        myDb.Entry(record).State = EntityState.Modified;
        return myDb.SaveChanges();
      }
    }

    /// <summary>
    /// getById
    /// </summary>
     public sys_menu getById(long id)
     {
       using (pwEntities myDb = new pwEntities())
       {
     // TODO 生成代码后需要检查一下是否找到正确的主键，这里做一个错误代码，避免直接使用
                  return myDb.Set<sys_menu>().Where<sys_menu>(p => p.menu_id == id).FirstOrDefault<sys_menu>();
                                                                                                                                                                                                                       

         }
      }
    }
}
