using System;
using System.Collections.Generic;

namespace PW.Common.ViewModel
{
    public class SysMenu
    {
                        private long _menuId;
        
        /// <summary>
        /// 菜单ID
        /// </summary>
        public long MenuId
        {
            get
            {
				return _menuId;
            }
            set
            {
				_menuId = value;
            }
        }
                        private string _menuName;
        
        /// <summary>
        /// 菜单名称
        /// </summary>
        public string MenuName
        {
            get
            {
				return _menuName;
            }
            set
            {
				_menuName = value;
            }
        }
                        private long _parentId;
        
        /// <summary>
        /// 父菜单ID
        /// </summary>
        public long ParentId
        {
            get
            {
				return _parentId;
            }
            set
            {
				_parentId = value;
            }
        }
                        private int _orderNum;
        
        /// <summary>
        /// 显示顺序
        /// </summary>
        public int OrderNum
        {
            get
            {
				return _orderNum;
            }
            set
            {
				_orderNum = value;
            }
        }
                        private string _path;
        
        /// <summary>
        /// 路由地址
        /// </summary>
        public string Path
        {
            get
            {
				return _path;
            }
            set
            {
				_path = value;
            }
        }
                        private string _component;
        
        /// <summary>
        /// 组件路径
        /// </summary>
        public string Component
        {
            get
            {
				return _component;
            }
            set
            {
				_component = value;
            }
        }
                        private int _isFrame;
        
        /// <summary>
        /// 是否为外链（0是 1否）
        /// </summary>
        public int IsFrame
        {
            get
            {
				return _isFrame;
            }
            set
            {
				_isFrame = value;
            }
        }
                        private int _isCache;
        
        /// <summary>
        /// 是否缓存（0缓存 1不缓存）
        /// </summary>
        public int IsCache
        {
            get
            {
				return _isCache;
            }
            set
            {
				_isCache = value;
            }
        }
                        private string _menuType;
        
        /// <summary>
        /// 菜单类型（M目录 C菜单 F按钮）
        /// </summary>
        public string MenuType
        {
            get
            {
				return _menuType;
            }
            set
            {
				_menuType = value;
            }
        }
                        private string _visible;
        
        /// <summary>
        /// 菜单状态（0显示 1隐藏）
        /// </summary>
        public string Visible
        {
            get
            {
				return _visible;
            }
            set
            {
				_visible = value;
            }
        }
                        private string _status;
        
        /// <summary>
        /// 菜单状态（0正常 1停用）
        /// </summary>
        public string Status
        {
            get
            {
				return _status;
            }
            set
            {
				_status = value;
            }
        }
                        private string _perms;
        
        /// <summary>
        /// 权限标识
        /// </summary>
        public string Perms
        {
            get
            {
				return _perms;
            }
            set
            {
				_perms = value;
            }
        }
                        private string _icon;
        
        /// <summary>
        /// 菜单图标
        /// </summary>
        public string Icon
        {
            get
            {
				return _icon;
            }
            set
            {
				_icon = value;
            }
        }
                        private string _createBy;
        
        /// <summary>
        /// 创建者
        /// </summary>
        public string CreateBy
        {
            get
            {
				return _createBy;
            }
            set
            {
				_createBy = value;
            }
        }
                        private DateTime _createTime;
        
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateTime
        {
            get
            {
				return _createTime;
            }
            set
            {
				_createTime = value;
            }
        }
                        private string _updateBy;
        
        /// <summary>
        /// 更新者
        /// </summary>
        public string UpdateBy
        {
            get
            {
				return _updateBy;
            }
            set
            {
				_updateBy = value;
            }
        }
                        private DateTime _updateTime;
        
        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime UpdateTime
        {
            get
            {
				return _updateTime;
            }
            set
            {
				_updateTime = value;
            }
        }
                        private string _remark;
        
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark
        {
            get
            {
				return _remark;
            }
            set
            {
				_remark = value;
            }
        }
            }
}