﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PW.Service.common
{
    [DataContract, Serializable]
    public class ServiceResult<T>
    {
        [DataMember]
        public bool success { get; set; }
        [DataMember]
        public string msg { get; set; }
        [DataMember]
        public T data { get; set; }
        [DataMember]
        public int code { get; set; }
        public ServiceResult(bool success, string msg, T data, int code)
        {
            this.success = success;
            this.msg = msg;
            this.data = data;
            this.code = code;
        }
        public ServiceResult(bool success, string msg, int code)
        {
            this.success = success;
            this.msg = msg;
            this.code = code;
        }
        public static ServiceResult<T> Success()
        {
            return ServiceResult<T>.Success("操作成功");
        }
        public static ServiceResult<T> Success(T data)
        {
            return ServiceResult<T>.Success("操作成功", data);
        }
        public static ServiceResult<T> Success(String msg)
        {
            return new ServiceResult<T>(true, msg, 200);
        }

        public static ServiceResult<T> Success(String msg, T data)
        {
            return new ServiceResult<T>(true, msg, data, 200);
        }
        public static ServiceResult<T> Error()
        {
            return ServiceResult<T>.Error("操作失败");
        }
        public static ServiceResult<T> Error(String msg)
        {
            return new ServiceResult<T>(false, msg, 500);
        }
        public static ServiceResult<T> Error(String msg, T data)
        {
            return new ServiceResult<T>(false, msg, data, 500);
        }

        public static ServiceResult<T> Error(int code, String msg)
        {
            return new ServiceResult<T>(false, msg, code);
        }


    }
}