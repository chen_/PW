# PW

 **留了一版纯前端的，没有数据库，没有接口，在这里** 
[https://gitee.com/shi2015/st-wpf-ui/tree/v1.0](https://gitee.com/shi2015/st-wpf-ui/tree/v1.0)

#### 介绍
wpf

#### 软件架构
软件架构说明
Visual Studio 2015
.NET Framework 4.6

Prism.Core 7.0
Prism.Mef 6.3
Prism.Wpf 6.3

EntityFramework 6.2
MySql.Data 6.10.9

#### 安装教程

1.  初始化NuGet包
2.  安装EF需要安装文件夹下程序
3.  单独生成各模块（Aside、Chat、Footer、SystemHeader(必须)、Map、SystemSet、Tools 三个模块生成一个主工程会加载一个模块的菜单）
(生成文件自动复制到PW\PW.Desktop\bin\Debug\DirectoryModules供启动项目加载，这些模块未在主项目中引用)
4.  运行PW.Desktop


数据库pw.sql
数据库连接：PW.Service  Web.config下 DBConnection 和 pwEntities

代码生成（2023-04-01）：
工具-》代码工具-》代码生成-》连接数据库-》选中数据表 -》生成
生成目录：PW\PW.Desktop\bin\Debug\CodeGenerator
生成代码复制到目录：
Dao: PW.DBCommon -> Dao
Model: PW.DBCommon -> Model  --暂未用
Service: PW.Service -> 
ServiceCenter: Common -> PW.ServiceCenter -> Dao
ViewModel: Modules -> PW.SystemSet -> ViewModel
Views: Modules -> PW.SystemSet -> Views

添加服务引用：
Common -> PW.ServiceCenter
注意生成异步操作
![输入图片说明](%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20230402145447.png)
![输入图片说明](%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20230402145503.png)

主项目下添加服务引用配置
PW.ServiceCenter->app.config 下找到新增的bindings 和 client
复制到
PW.Desktop -> App.config
修改 endpoint 添加属性 
behaviorConfiguration="AuthenticationBehavior"


添加菜单：
Modules -> PW.SystemSet -> SystemSetModule.cs -> Initialize

PW.SystemHeader 右键生成项目（生成代码页面所在模块）
启动项目

接口安全：
PW.Service -> Web.config 下
AuthenticationBehavior
默认注释掉的，开启后客户端添加或更新服务引用会报错

代码生成遗留问题
1.生成代码存在id类型long  和 int不匹配问题  生成后自行修改
2.生成代码查询表单和编辑页表单布局问题  后续修改

#### 预览
[https://blog.csdn.net/shishuwei111/article/details/82933075](https://blog.csdn.net/shishuwei111/article/details/82933075)
![登录](https://images.gitee.com/uploads/images/2020/0727/135659_41bccf8d_545745.png "20181003180923780.png")
![主导航](https://images.gitee.com/uploads/images/2020/0727/135737_75a7071c_545745.png "20181003181111256.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0727/135759_559ad54e_545745.png "20181003181145876.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0727/135809_0e59e5a4_545745.png "20181003181214849.png")

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
